package com.example.spring.redis.test;

import com.example.spring.redis.model.Person;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ScanOptions;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class HashOperationTest extends BaseTest {

    final UUID personId = UUID.randomUUID();
    final String key = "persons:" + personId;
    HashOperations<String, String, Object> hashOperations;

    @BeforeEach
    @DisplayName( "Person 인스턴스를 해시맵 타입으로 redis에 저장" )
    void save_person() throws IllegalAccessException {
        Person person = Person.builder()
                .id( personId )
                .personName( "test-name" )
                .personAge( 100 )
                .personNation( "korea" )
                .comment( "first commit data" )
                .build();

        Map<String, Object> personMap = person.createMap();
        /* getMap 대신에 reflection을 이용하여 저장할 수 있다.
        Map<String, Object> personMap = new HashMap<>();
        Class<? extends Person> personClass = person.getClass();
        Field[] fields = personClass.getDeclaredFields();
        for ( Field field : fields ) {
            field.setAccessible( true );
            personMap.put( field.getName(), field.get( person ) );
        }
        */
        //person 저장
        hashOperations = redisTemplate.opsForHash();
        hashOperations.putAll( key, personMap );
    }

    @Test
    @DisplayName( "특정 해시맵의 엔트리 삭제 + 해시 key 체크 테스트 - comment, personNation 필드 삭제" )
    void delete_haskey_test() {
        final List<String> deleteHashKeyList = List.of( "comment", "personNation" );

        Long deleteCount = hashOperations.delete( key, "comment", "personNation" );
        Assertions.assertThat( deleteCount ).isNotNull();
        Assertions.assertThat( deleteCount ).isEqualTo( deleteHashKeyList.size() );

        Boolean exists = hashOperations.hasKey( key, deleteHashKeyList.getFirst() );
        Assertions.assertThat( exists ).isFalse();
        exists = hashOperations.hasKey( key, deleteHashKeyList.getLast() );
        Assertions.assertThat( exists ).isFalse();
    }

    @Test
    @DisplayName( "해시맵 엔트리 목록을 가져오는 테스트" )
    void entries_test() {
        Map<String, Object> entries = hashOperations.entries( key );
        Assertions.assertThat( entries ).isNotNull();
        Assertions.assertThat( entries.size() ).isEqualTo( 5 );
        Assertions.assertThat( entries.get( "id" ) ).isEqualTo( personId.toString() );
    }

    @Test
    @DisplayName( "특정 해시맵 엔트리의 값을 조회하는 테스트" )
    void get_test() {
        Object hashValue = hashOperations.get( key, "id" );
        Assertions.assertThat( hashValue ).isNotNull();
        Assertions.assertThat( hashValue ).isEqualTo( personId.toString() );

        //조회하는 해시 키가 없는 경우 null을 리턴한다.
        hashValue = hashOperations.get( key, "noField" );
        Assertions.assertThat( hashValue ).isNull();
    }

    @Test
    @DisplayName( "정수형 혹은 소수점 타입의 특정 엔트리의 값을 증가시키는 테스트" )
    void increment_test() {
        //현재 personAge는 100인 상태에서 personAge값을 10 증가시키고 증가된 값을 리턴한다.
        Long personAge = hashOperations.increment( key, "personAge", 10 );
        Object hashValue = hashOperations.get( key, "personAge" );
        Assertions.assertThat( hashValue ).isNotNull();
        Assertions.assertThat( hashValue ).isInstanceOf( Integer.class );
        Assertions.assertThat( hashValue ).isEqualTo( personAge.intValue() );
    }

    @Test
    @DisplayName( "해시맵의 모든 엔트리의 키 목록을 조회하는 테스트" )
    void keys_test() {
        Set<String> hashKeys = hashOperations.keys( key );
        Assertions.assertThat( hashKeys ).isNotNull();
        Assertions.assertThat( hashKeys.size() ).isEqualTo( 5 );
        Assertions.assertThat( hashKeys.contains( "id" ) ).isTrue();
        Assertions.assertThat( hashKeys.contains( "personName" ) ).isTrue();
        Assertions.assertThat( hashKeys.contains( "personAge" ) ).isTrue();
        Assertions.assertThat( hashKeys.contains( "personNation" ) ).isTrue();
        Assertions.assertThat( hashKeys.contains( "comment" ) ).isTrue();
    }

    @Test
    @DisplayName( "해시맵에 엔트리를 추가하거나 엔트리의 값을 수정하는 테스트" )
    void put_test() {
        hashOperations.put( key, "personName", "modified-name" );
        Object hashValue = hashOperations.get( key, "personName" );
        Assertions.assertThat( hashValue ).isNotNull();
        Assertions.assertThat( hashValue ).isEqualTo( "modified-name" );

        //존재하지 않는 엔트리 키에 대해서 put을 하면 새로운 엔트리를 추가한다.
        hashOperations.put( key, "noField", "this field added" );
        hashValue = hashOperations.get( key, "noField" );
        Assertions.assertThat( hashValue ).isNotNull();
        Assertions.assertThat( hashValue ).isEqualTo( "this field added" );
    }

    @Test
    @DisplayName( "해시맵에 엔트리 키가 존재하지 않는 경우에만 엔트리를 추가하는 테스트" )
    void putIfAbsent_test() {
        //personName 키가 존재하므로 putIfAbsent 결과는 false다.
        Boolean result = hashOperations.putIfAbsent( key, "personName", "modified-name" );
        Object hashValue = hashOperations.get( key, "personName" );
        Assertions.assertThat( result ).isFalse();
        Assertions.assertThat( hashValue ).isNotEqualTo( "modified-name" );

        //존재하지 않는 엔트리 키에 대해서 putIfAbsent 결과는 true다.
        result = hashOperations.putIfAbsent( key, "noField", "this field added" );
        Assertions.assertThat( result ).isTrue();
        hashValue = hashOperations.get( key, "noField" );
        Assertions.assertThat( hashValue ).isNotNull();
        Assertions.assertThat( hashValue ).isEqualTo( "this field added" );
    }

    @Test
    @DisplayName( "해시맵의 엔트리 수를 조회하는 테스트" )
    void size_test() {
        Long size = hashOperations.size( key );
        Assertions.assertThat( size ).isNotNull();
        Assertions.assertThat( size ).isEqualTo( 5L );
    }

    @Test
    @DisplayName( "해시의 엔트리에 저장된 값 목록을 조회하는 테스트" )
    void values_test() {
        List<Object> values = hashOperations.values( key );
        /*
        Person person = Person.builder()
                .id( personId )
                .personName( "test-name" )
                .personAge( 100 )
                .personNation( "korea" )
                .comment( "first commit data" )
                .build();
         */
        Assertions.assertThat( values ).isNotNull();
        Assertions.assertThat( values.size() ).isEqualTo( 5 );
    }

    @Test
    @DisplayName( "scan() 테스트" )
    void scan_test() {
        ScanOptions scanOptions = ScanOptions.scanOptions().match( "person*" ).build();
        try( Cursor<Map.Entry<String, Object>> cursor =
                     hashOperations.scan( key, scanOptions ) ) {
            while ( cursor.hasNext() ) {
                Map.Entry<String, Object> next = cursor.next();
                System.out.println("key: " + next.getKey() + " value: " + next.getValue());
                switch ( next.getKey() ) {
                    case "id" ->
                            Assertions.assertThat( next.getValue() ).isEqualTo( personId.toString() );
                    case "personName" ->
                            Assertions.assertThat( next.getValue() ).isEqualTo( "test-name" );
                    case "personAge" ->
                            Assertions.assertThat( next.getValue() ).isEqualTo( 100 );
                    case "personNation" ->
                            Assertions.assertThat( next.getValue() ).isEqualTo( "korea" );
                    case "comment" ->
                            Assertions.assertThat( next.getValue() ).isEqualTo( "first commit data" );
                }
            }
        }
    }

    @Test
    @DisplayName( "randomEntries, randomEntry, randomKey, randomKeys 테스트" )
    void random_method_test() {
        Map<String, Object> randomEntries = hashOperations.randomEntries( key, 2 );
        Assertions.assertThat( randomEntries ).isNotNull();
        Assertions.assertThat( randomEntries.size() ).isEqualTo( 2 );

        Map.Entry<String, Object> entry = hashOperations.randomEntry( key );
        Assertions.assertThat( entry ).isNotNull();

        Set<String> keys = hashOperations.keys( key );
        String randomKey = hashOperations.randomKey( key );
        Assertions.assertThat( randomKey ).isNotNull();
        Assertions.assertThat( keys ).contains( randomKey );

        List<String> randomKeys = hashOperations.randomKeys( key, 2 );
        Assertions.assertThat( randomKeys ).isNotNull();
        Assertions.assertThat( randomKeys.size() ).isEqualTo( 2 );
        Assertions.assertThat( keys ).containsAll( randomKeys );
    }
}
