package com.example.spring.redis.test;

import com.example.spring.redis.model.Person;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

public class RedisTest extends BaseTest {

    final UUID id = UUID.randomUUID();

    @Test
    @DisplayName( "redis insert test" )
    void insert_person_redis_test() {
        Person person = Person.builder()
                .id( id )
                .personName( "test-name" )
                .personAge( 100 )
                .personNation( "korea" )
                .comment( "first commit data" )
                .build();

        redisTemplate.opsForValue().set( id.toString(), person );

        // find data
        Object object = redisTemplate.opsForValue().get( id.toString() );
        Assertions.assertThat( object ).isInstanceOf( Person.class );
        Person find = ( Person ) object;
        Assertions.assertThat( find.getId() ).isEqualTo( id );
    }

    @Test
    @DisplayName( "redis string insert test" )
    void insert_string_redis_test() {
        final String name = "test";
        final String value = "test-value";
        redisTemplate.opsForValue().set( name, value );

        // find data
        Object object = redisTemplate.opsForValue().get( name );
        Assertions.assertThat( object ).isInstanceOf( String.class );
        String find = ( String ) object;
        Assertions.assertThat( find ).isEqualTo( value );
    }
}
