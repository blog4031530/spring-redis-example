package com.example.spring.redis.test;

import com.example.spring.redis.model.Person;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.ValueOperations;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class RedisTemplateTest extends BaseTest {

    @Test
    @DisplayName( "ValueOperations 테스트" )
    void value_operation_test() {
        final UUID id = UUID.randomUUID();
        Person person = Person.builder()
                .id( id )
                .personName( "test-name" )
                .personAge( 100 )
                .personNation( "korea" )
                .comment( "first commit data" )
                .build();

        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        valueOperations.set( id.toString(), person );

        // find data
        Object object = valueOperations.get( id.toString() );
        Assertions.assertThat( object ).isInstanceOf( Person.class );
        if ( object instanceof Person personInstance ) {
            Assertions.assertThat( personInstance.getId() ).isEqualTo( id );
        }
    }

    @Test
    @DisplayName( "HashOperations 테스트" )
    void hash_operation_test() {
        final UUID id = UUID.randomUUID();
        Person person = Person.builder()
                .id( id )
                .personName( "test-name" )
                .personAge( 100 )
                .personNation( "korea" )
                .comment( "first commit data" )
                .build();

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> map = objectMapper.convertValue( person, Map.class );

        HashOperations<String, String, Object> hashOperations = redisTemplate.opsForHash();
        hashOperations.putAll( id.toString(), map );

        map.keySet().forEach( elementKey -> {
            Object o = hashOperations.get( id.toString(), elementKey );
            switch ( elementKey ) {
                case "id" -> {
                    Assertions.assertThat( o ).isInstanceOf( String.class );
                    Assertions.assertThat( (String) o ).isEqualTo( id.toString() );
                }
                case "personName" -> {
                    Assertions.assertThat( o ).isInstanceOf( String.class );
                    Assertions.assertThat( (String) o ).isEqualTo( "test-name" );
                }
                case "personAge" -> {
                    Assertions.assertThat( o ).isInstanceOf( Integer.class );
                    Assertions.assertThat( (Integer ) o ).isEqualTo( 100 );
                }
                case "personNation" -> {
                    Assertions.assertThat( o ).isInstanceOf( String.class );
                    Assertions.assertThat( (String) o ).isEqualTo( "korea" );
                }
                case "comment" -> {
                    Assertions.assertThat( o ).isInstanceOf( String.class );
                    Assertions.assertThat( (String) o ).isEqualTo( "first commit data" );
                }
                default -> throw new IllegalArgumentException( "Unsupported hash operation key: " + elementKey );
            }
        } );
    }

    @Test
    @DisplayName( "ListOperations 테스트" )
    void list_operation_test() {
        final String listKey = UUID.randomUUID().toString();
        final UUID id1 = UUID.randomUUID();
        final UUID id2 = UUID.randomUUID();

        List<Person> personList = List.of(
                Person.builder()
                        .id( id1 )
                        .personName( "list-person1" )
                        .personAge( 10 )
                        .personNation( "korea" )
                        .comment( "first commit data" )
                        .build(),
                Person.builder()
                        .id( id2 )
                        .personName( "list-person2" )
                        .personAge( 20 )
                        .personNation( "America" )
                        .comment( "second commit data" )
                        .build()
        );

        ListOperations<String, Object> listOperations = redisTemplate.opsForList();
        //Redis 리스트의 오른쪽으로 personList가 append 된다.
        //주의할 것은 redisTemplate의 Value 타입이 Object 이므로 personList List 객체를 넘기는 경우
        //personList에 있는 각각의 Person 객체가 삽입되는게 아닌
        //personList가 하나의 Object로 삽입된다.
        //그렇게 되면 아래와 같이 rightPushAll 메서드를 호출할 하게 되면
        //데이터를 가져올 때 deserialization 관련하여 오류가 발생한다.
        //listOperations.rightPushAll( listKey, personList );
        //위와 같은 이유로 personList.toArray()로 array 형태로 저장하거나 personList내의 Person 객체를 하나씩 넣어야 한다.
        //personList를 그대로 전달하도록 하려면 Value 타입이 Person 타입으로 지정된 별도의 RedisTemplate 빈을 생성하여 처리한다.
        //그에 대한 테스트 코드는 list_operation_person_value_type_test() 에서 확인할 수 있다.
        listOperations.rightPushAll( listKey, personList.toArray() );

        //지정된 범위의 list를 가져온다. personList.size() 대신에 -1을 지정해도 된다.
        List<Object> rangeList = listOperations.range( listKey, 0, -1 );
        Assertions.assertThat( rangeList ).isNotNull();
        Assertions.assertThat( rangeList ).hasSize( 2 );

        //Redis에 저장된 리스트의 첫번째 인덱스의 Person 인스턴스를 조회한다.
        Object personObject = listOperations.index( listKey, 0 );
        Assertions.assertThat( personObject ).isNotNull();
        Assertions.assertThat( personObject ).isInstanceOf( Person.class );
        Assertions.assertThat( ((Person) personObject).getId() ).isEqualTo( id1 );
    }

    @Test
    @DisplayName( "Person Value Type ListOperations 테스트" )
    void list_operation_person_value_type_test() {
        final String listKey = UUID.randomUUID().toString();
        final UUID id1 = UUID.randomUUID();
        final UUID id2 = UUID.randomUUID();

        List<Person> personList = List.of(
                Person.builder()
                        .id( id1 )
                        .personName( "list-person1" )
                        .personAge( 10 )
                        .personNation( "korea" )
                        .comment( "first commit data" )
                        .build(),
                Person.builder()
                        .id( id2 )
                        .personName( "list-person2" )
                        .personAge( 20 )
                        .personNation( "America" )
                        .comment( "second commit data" )
                        .build()
        );

        ListOperations<String, Person> listOperations = personRedisTemplate.opsForList();
        //Value 타입이 Person으로 특화된 personRedisTemplate을 사용하므로 personList 자체를 전달할 수 있다.
        listOperations.rightPushAll( listKey, personList );

        //지정된 범위의 list를 가져온다. personList.size() 대신에 -1을 지정해도 된다.
        List<Person> rangeList = listOperations.range( listKey, 0, -1 );
        Assertions.assertThat( rangeList ).isNotNull();
        Assertions.assertThat( rangeList ).hasSize( 2 );

        //Redis에 저장된 리스트의 첫번째 인덱스의 Person 인스턴스를 조회한다.
        Object personObject = listOperations.index( listKey, 0 );
        Assertions.assertThat( personObject ).isNotNull();
        Assertions.assertThat( personObject ).isInstanceOf( Person.class );
        Assertions.assertThat( ((Person) personObject).getId() ).isEqualTo( id1 );
    }
}
