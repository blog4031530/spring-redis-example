package com.example.spring.redis.test;

import com.example.spring.redis.model.Person;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.data.redis.core.ValueOperations;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class ValueOperationTest extends BaseTest {

    @Test
    @DisplayName( "set and get 테스트" )
    void set_and_get_test() {
        final UUID key = UUID.randomUUID();
        final UUID id = UUID.randomUUID();
        Person person = Person.builder()
                .id( id )
                .personName( "test-name" )
                .personAge( 100 )
                .personNation( "korea" )
                .comment( "first commit data" )
                .build();

        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        valueOperations.set( key.toString(), person );

        // find data
        Object object = valueOperations.get( key.toString() );
        Assertions.assertThat( object ).isInstanceOf( Person.class );
        if ( object instanceof Person personInstance ) {
            Assertions.assertThat( personInstance.getId()).isEqualTo( id );
        }

        //void set(K key, V value, long offset) 테스트
        final UUID key2 = UUID.randomUUID();
        final String value = "test data";
        ValueOperations<String, String> stringValueOperations = stringRedisTemplate.opsForValue();
        stringValueOperations.set( key2.toString(), value, 0L );
        stringValueOperations.set( key2.toString(), " my data", 4 );
        String getValue = stringValueOperations.get( key2.toString() );
        Assertions.assertThat( getValue ).isEqualTo( "test my data" );
        getValue = stringValueOperations.get( key2.toString(), 0, 6 );
        Assertions.assertThat( getValue ).isEqualTo( "test my" );
    }

    @Test
    @DisplayName( "set and get expiration 테스트" )
    void set_and_expiration_test() throws InterruptedException {
        final UUID key = UUID.randomUUID();
        final UUID id = UUID.randomUUID();
        Person person = Person.builder()
                .id( id )
                .personName( "test-name" )
                .personAge( 100 )
                .personNation( "korea" )
                .comment( "first commit data" )
                .build();

        //5초 후에 데이터 만료 - 5초 후에 Redis에 저장된 데이터는 자동 삭제된다.
        final long timeout = 5000L;
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        //아래 메서드를 호출해도 동일하다.
        //valueOperations.set( key.toString(), person, Duration.ofMillis(timeout) );
        valueOperations.set( key.toString(), person, timeout, TimeUnit.MILLISECONDS );

        // find data
        Object object = valueOperations.get( key.toString() );
        Assertions.assertThat( object ).isNotNull();
        Assertions.assertThat( object ).isInstanceOf( Person.class );
        if ( object instanceof Person personInstance ) {
            Assertions.assertThat( personInstance.getId() ).isEqualTo( id );
        }

        //5초가 지난뒤에는 데이터가 삭제됨
        Thread.sleep( timeout );
        object = valueOperations.get( key.toString() );
        Assertions.assertThat( object ).isNull();

        //getAndExpire 테스트
        final UUID key2 = UUID.randomUUID();
        final String value = "test value";
        ValueOperations<String, String> stringValueOperations = stringRedisTemplate.opsForValue();
        stringValueOperations.set( key2.toString(), value );
        String getValue = stringValueOperations.getAndExpire( key2.toString(), Duration.ofMillis( timeout ) );
        Assertions.assertThat( getValue ).isEqualTo( value );
        Thread.sleep( timeout );
        getValue = stringValueOperations.get( key2.toString() );
        Assertions.assertThat( getValue ).isNull();
    }

    @Test
    @DisplayName( "getAndSet 테스트" )
    void getAndSet_test() {
        final UUID key = UUID.randomUUID();
        final UUID oldPersonId = UUID.randomUUID();
        Person person = Person.builder()
                .id( oldPersonId )
                .personName( "test-name" )
                .personAge( 100 )
                .personNation( "korea" )
                .comment( "first commit data" )
                .build();

        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        //현재 value에는 데이터가 없으므로 object는 null 이고 person이 value에 저장됨.
        Object object = valueOperations.getAndSet( key.toString(), person );
        Assertions.assertThat( object ).isNull();

        //person 저장 검증
        object = valueOperations.get( key.toString() );
        Assertions.assertThat( object ).isNotNull();
        Assertions.assertThat( object ).isInstanceOf( Person.class );
        if ( object instanceof Person personInstance ) {
            Assertions.assertThat( personInstance.getId() ).isEqualTo( oldPersonId );
        }

        UUID newPersonId = UUID.randomUUID();
        person = Person.builder()
                .id( newPersonId )
                .personName( "new-name" )
                .personAge( 101 )
                .personNation( "korea" )
                .comment( "new commit data" )
                .build();
        //새로운 person 인스턴스를 저장하고 이전에 저장된 person 객체를 리턴한다.
        object = valueOperations.getAndSet( key.toString(), person );
        Assertions.assertThat( object ).isNotNull();
        Assertions.assertThat( object ).isInstanceOf( Person.class );
        if ( object instanceof Person personInstance ) {
            Assertions.assertThat( personInstance.getId() ).isEqualTo( oldPersonId );
        }
    }

    @Test
    @DisplayName( "setIfAbsent 테스트" )
    void setIfAbsent_test() {
        final UUID key = UUID.randomUUID();
        final UUID personId = UUID.randomUUID();
        Person person = Person.builder()
                .id( personId )
                .personName( "test-name" )
                .personAge( 100 )
                .personNation( "korea" )
                .comment( "first commit data" )
                .build();

        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        //현재 key가 저장되지 않은 상태이므로 true를 리턴함.
        //setIfAbsent(key, value, timeout, timeunit) 만료시간을 지정할 수도 있다.
        //setIfAbsent(key, value, Duration.ofMillis(timeout)) 만료시간을 지정할 수도 있다.
        Boolean result = valueOperations.setIfAbsent( key.toString(), person );
        Assertions.assertThat( result ).isTrue();

        //person 저장 검증
        Object object = valueOperations.get( key.toString() );
        Assertions.assertThat( object ).isNotNull();
        Assertions.assertThat( object ).isInstanceOf( Person.class );
        if ( object instanceof Person personInstance ) {
            Assertions.assertThat( personInstance.getId() ).isEqualTo( personId );
        }

        UUID newPersonId = UUID.randomUUID();
        Person newPerson = Person.builder()
                .id( newPersonId )
                .personName( "new-name" )
                .personAge( 101 )
                .personNation( "korea" )
                .comment( "new commit data" )
                .build();
        //key가 이미 존재하므로 setIfAbsent()의 결과는 false가 됨.
        result = valueOperations.setIfAbsent( key.toString(), newPerson );
        Assertions.assertThat( result ).isFalse();
    }

    @Test
    @DisplayName( "setIfPresent 테스트" )
    void setIfPresent_test() {
        final UUID key = UUID.randomUUID();
        final UUID personId = UUID.randomUUID();
        Person person = Person.builder()
                .id( personId )
                .personName( "test-name" )
                .personAge( 100 )
                .personNation( "korea" )
                .comment( "first commit data" )
                .build();

        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        //현재 key가 존재하지 않은 상태이므로 false를 리턴함. person 인스턴스는 저장되지 않음.
        Boolean result = valueOperations.setIfPresent( key.toString(), person );
        Assertions.assertThat( result ).isFalse();

        //key에 person 인스턴스를 저장
        valueOperations.set( key.toString(), person );

        //person 저장 검증
        Object object = valueOperations.get( key.toString() );
        Assertions.assertThat( object ).isNotNull();
        Assertions.assertThat( object ).isInstanceOf( Person.class );

        if ( object instanceof Person personInstance ) {
            Assertions.assertThat( personInstance.getId() ).isEqualTo( personId );
            personInstance.setPersonName( "modified-name" );
            result = valueOperations.setIfPresent( key.toString(), personInstance );
            Assertions.assertThat( result ).isTrue();

            object = valueOperations.get( key.toString() );
            Assertions.assertThat( object ).isNotNull();
            Assertions.assertThat( object ).isInstanceOf( Person.class );

            Assertions.assertThat( ((Person) object).getPersonName() ).isEqualTo( "modified-name" );
        }
    }

    @Test
    @DisplayName( "increment 테스트" )
    void increment_test() {
        final UUID key = UUID.randomUUID();
        final int value = 100;

        ValueOperations<String, Integer> valueOperations = integerRedisTemplate.opsForValue();
        valueOperations.set( key.toString(), value );
        Integer object = valueOperations.get( key.toString() );
        Assertions.assertThat( object ).isNotNull();
        Assertions.assertThat( value ).isEqualTo( object );

        final int delta = 10;
        //redis에 저장된 값을 10만큼 증가 시킴
        Long result = valueOperations.increment( key.toString(), delta );
        Integer incrementObject = valueOperations.get( key.toString() );
        Assertions.assertThat( incrementObject ).isNotNull();

        Assertions.assertThat( value + delta ).isEqualTo( incrementObject );
        Assertions.assertThat( result.intValue() ).isEqualTo( incrementObject );
    }

    @Test
    @DisplayName( "append and size 테스트" )
    void append_test() {
        final String key = UUID.randomUUID().toString();
        final String value = "string value";
        final String append = ", appended data";

        ValueOperations<String, String> valueOperations = stringRedisTemplate.opsForValue();
        valueOperations.set( key, value );
        String getValue = valueOperations.get( key );
        Assertions.assertThat( getValue ).isEqualTo( value );

        //문자열을 추가한다.
        //appendedSize는 추가된 문자열을 포함한 길이를 리턴한다.
        Integer appendedSize = valueOperations.append( key, append );
        getValue = valueOperations.get( key );
        Assertions.assertThat( getValue ).isEqualTo( "%s%s", value, append );

        Long size = valueOperations.size( key );
        Assertions.assertThat( size ).isEqualTo( String.format( "%s%s", value, append ).length() );
        Assertions.assertThat( size.intValue() ).isEqualTo( appendedSize );
    }

    @Test
    @DisplayName( "multiSet and multiGet 테스트" )
    void multiSet_and_multiGet_test() {
        List<String> keyList = List.of(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString()
        );

        List<UUID> personIdList = List.of(
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        Map<String, Person> personMap = Map.ofEntries(
                Map.entry( keyList.get( 0 ),
                        Person.builder()
                                .id( personIdList.getFirst() )
                                .personName( "test-name1" )
                                .personAge( 10 )
                                .personNation( "korea" )
                                .comment( "commit data1" )
                                .build() ),
                Map.entry( keyList.get( 1 ),
                        Person.builder()
                                .id( personIdList.getLast() )
                                .personName( "test-name2" )
                                .personAge( 11 )
                                .personNation( "America" )
                                .comment( "commit data2" )
                                .build() )
        );

        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        valueOperations.multiSet( personMap );

        //multiGet 메서드에 전달된 keyList에 있는 키 개수는 3개다.
        //multiSet 메서드를 통해서 저장된 personMap 은 2개다.
        //personList 크기는 keyList 수와 동일하지만 redis에 저장되지 않은 keyList에 있는 하나의 key에 대한 결과는 null 이다.
        List<Object> personList = valueOperations.multiGet( keyList );

        Assertions.assertThat( personList ).isNotNull();
        //존재하지 않는 key가 keyList에 포함되어 있는 경우 해당 value는 null로 출력된다.
        Assertions.assertThat( personList.size() ).isEqualTo( keyList.size() );
        Assertions.assertThat( personList ).contains( ( Object )null );

        personList.stream()
                .filter( Objects::nonNull )
                .forEach( person -> {
                    Assertions.assertThat( person ).isInstanceOf( Person.class );
                    Assertions.assertThat( personIdList.contains( ((Person) person).getId() ) ).isTrue();
                } );
    }

    @Test
    @DisplayName( "multiSetIfAbsent 테스트" )
    void multiSetIfAbsent_test() {
        List<String> keyList = List.of(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString()
        );

        List<UUID> personIdList = List.of(
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        //multiSetIfAbsent 호출시 이미 존재하는 key가 있는 경우 동작 확인을 위해 firstPerson 객체를 우선 저장한다.
        Person firstPerson = Person.builder()
                .id( personIdList.getFirst() )
                .personName( "test-name1" )
                .personAge( 10 )
                .personNation( "korea" )
                .comment( "commit data1" )
                .build();

        Person secondPerson = Person.builder()
                .id( personIdList.getLast() )
                .personName( "test-name2" )
                .personAge( 11 )
                .personNation( "America" )
                .comment( "commit data2" )
                .build();
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        valueOperations.set( keyList.getFirst(), firstPerson );

        Map<String, Person> personMap = Map.ofEntries(
                Map.entry( keyList.getFirst(), firstPerson ),
                Map.entry( keyList.getLast(), secondPerson )
        );

        //multiSetIfAbsent로 전달되는 Map의 모든 Key가 존재하지 않아야 결과는 성공한다.
        Boolean result = valueOperations.multiSetIfAbsent( personMap );
        Assertions.assertThat( result ).isFalse();

        //multiSetIfAbsent 결과가 실패했으므로 keyList 에 대응하는 List 결과는 크기는 2 이지만 secondPerson은 저장되지 않았으므로
        //하나는 이미 저장되었던 firstPerson 인스턴스이고 나머지 하나는 null 이다.
        List<Object> personList = valueOperations.multiGet( keyList );

        Assertions.assertThat( personList ).isNotNull();
        Assertions.assertThat( personList.size() ).isEqualTo( keyList.size() );
        Assertions.assertThat( personList ).contains( ( Object )null );

        personList.stream()
                .filter( Objects::nonNull )
                .forEach( person -> {
                    Assertions.assertThat( person ).isInstanceOf( Person.class );
                    Assertions.assertThat( personIdList.contains( ((Person) person).getId() ) ).isTrue();
                } );
    }

    //키에 저장된 문자열 값의 오프셋 비트를 설정하거나 지운다.
    //Redis의 문자열은 바이너리 값에 안전하기 때문에 비트맵은 바이트 스트림으로 간단히 인코딩된다.
    //문자열의 첫번째 바이트는 비트맵의 오프셋 0..7, 두번째 바이트는 8..15 범위 등에 해당된다.
    @Test
    @DisplayName( "setBit and getBit 테스트" )
    void setBit_and_getBit_test() {
        ///////////////////////////////////////////////////////////////////////////////
        // "42"값을 가진 byte 배열을 저장 후 setBit() 메서드를 통해서 "44"값으로 변경하는 테스트
        ///////////////////////////////////////////////////////////////////////////////
        final String key = UUID.randomUUID().toString();
        String value = "42"; //0011_0100('4') 0011_0010('2')

        long setBitPosition = 13;
        long unsetBitPosition = 14;
        ValueOperations<String, String> valueOperations = stringRedisTemplate.opsForValue();
        valueOperations.set( key, value );
        valueOperations.setBit( key, setBitPosition, true );
        valueOperations.setBit( key, unsetBitPosition, false );

        String getValue = valueOperations.get( key );
        Assertions.assertThat( getValue ).isEqualTo( "44" );


        ///////////////////////////////////////////////////////////////////////////////
        // 2개의 길이를 가진 byte배열(16bit)를 초기화 후 5번째 bit를 1로 변경 후 확인 테스트
        ///////////////////////////////////////////////////////////////////////////////
        //16개의 bit를 가지는 bit set을 정의. 초기 값은 모두 0.
        final String key2 = UUID.randomUUID().toString();
        //2byte(16bit) 의 공간을 0으로 초기화 하기 위함.
        byte[] bitArray = new byte[2];
        String bitString = new String( bitArray );

        setBitPosition = 5;
        valueOperations.set( key2, bitString );

        getValue = valueOperations.get( key2 );
        Assertions.assertThat( getValue ).isEqualTo( bitString );

        byte[] bytes = getValue.getBytes();
        Assertions.assertThat( bytes[0] ).isEqualTo( (byte) 0b0000_0000 );
        Assertions.assertThat( bytes[1] ).isEqualTo( (byte) 0b0000_0000 );

        Boolean bit = valueOperations.getBit( key2, setBitPosition );
        Assertions.assertThat( bit ).isEqualTo( false );

        valueOperations.setBit( key2, setBitPosition, true );
        bit = valueOperations.getBit( key2, setBitPosition );
        Assertions.assertThat( bit ).isEqualTo( true );

        //bit position이 5이므로 0..7 범위의 첫번째 바이트의 5번째 bit가 1로 변경된 것이다.
        getValue = valueOperations.get( key2 );
        bytes = getValue.getBytes();
        Assertions.assertThat( bytes[0] ).isEqualTo( (byte) 0b0000_0100 );
    }

    @Test
    @DisplayName( "setBit and getBit 테스트" )
    void setBit_and_getBit_test2() {
        final String key = UUID.randomUUID().toString();

        //초기 bitset의 크기는 (index / 8) + 1 byte로 (14 / 8) + 1 = 2 byte로 가장 큰 index 기준으로 동적으로 할당된다.
        ValueOperations<String, String> valueOperations = stringRedisTemplate.opsForValue();
        //0011_0100('4') 0011_0010('2')
        valueOperations.setBit( key, 2, true );
        valueOperations.setBit( key, 3, true );
        valueOperations.setBit( key, 5, true );
        valueOperations.setBit( key, 10, true );
        valueOperations.setBit( key, 11, true );
        valueOperations.setBit( key, 14, true );

        String getValue = valueOperations.get( key );
        Assertions.assertThat( getValue ).isEqualTo( "42" );

        //2번째 bit가 true로 지정되었기 때문에 결과는 true.
        Boolean result = valueOperations.getBit( key, 2 );
        Assertions.assertThat( result ).isTrue();

        //4번째 bit는 true로 지정되지 않았기 때문에 결과는 false.
        result = valueOperations.getBit( key, 4 );
        Assertions.assertThat( result ).isFalse();
    }
}
