package com.example.spring.redis.test;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.SessionCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TransactionTest extends BaseTest {

    @Test
    @DisplayName( "주문 / 재고 트랜잭션 테스트" )
    void order_stock_transaction_test() {
        final String productId = UUID.randomUUID().toString();
        //현재 재고 수량은 10개
        final Integer initialStock = 10;
        //구매 수량은 3
        final Integer purchaseCount = 3;

        //재고 초기 수량 저장
        integerRedisTemplate.opsForValue().set( "stock#" + productId, initialStock );

        try {
            //executed 인스턴스에서는 executeTransaction 메서드 내부에서 수행된 redis 명령의 결과가 순차적으로 저장된다.
            List<Object> executed = executeTransaction( productId, purchaseCount, false );
            if ( executed != null ) {
                Assertions.assertThat( executed.size() ).isEqualTo( 2 );
                //총 구매 수량 검증
                Assertions.assertThat( executed.getFirst() ).isEqualTo( purchaseCount.longValue() );
                System.out.println("남은 재고 수량: " + executed.getLast());
            } else {
                System.out.println("남은 재고가 부족합니다.!!!");
            }
        } catch ( DataAccessException e ) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName( "주문 / 재고 트랜잭션 테스트 with watch()" )
    void order_stock_transaction_watch_test() {
        final String productId = UUID.randomUUID().toString();
        //현재 재고 수량은 10개
        final Integer initialStock = 10;
        //구매 수량은 3
        final Integer purchaseCount = 3;

        //재고 초기 수량 저장
        integerRedisTemplate.opsForValue().set( "stock#" + productId, initialStock );

        try {
            List<Object> executed;

            //watch()로 인해 감시되는 key의 값이 트랜잭션 도중에 다른 connection의 명령에 의해서 변경된 경우
            //데이터 일관성이 꺠졌으므로 트랜잭션을 다시 수행한다.
            do {
                executed = executeTransaction( productId, purchaseCount, true );
                //executed 리스트에는
                //integerRedisTemplate.opsForValue().increment( "order#" + productId, purchaseCount );
                //integerRedisTemplate.opsForValue().decrement( "stock#" + productId, purchaseCount );
                //명령 수행 결과가 저장된다.
                if ( executed != null && !executed.isEmpty()) {
                    Assertions.assertThat( executed.size() ).isEqualTo( 2 );
                    //총 구매 수량 검증
                    Assertions.assertThat( executed.getFirst() ).isEqualTo( purchaseCount.longValue() );
                    System.out.println("남은 재고 수량: " + executed.getLast());
                }
            } while (executed != null && executed.isEmpty());

            if ( executed == null ) {
                System.out.println("남은 재고가 부족합니다.!!!");
            }
        } catch ( DataAccessException e ) {
            System.out.println(e.getMessage());
        }
    }

    // 주문 / 재고 트랜잭션
    // 현재 재고 수량에서 purchaseCount만큼 감소
    // 현재 구매 수량에서 purchaseCount만큼 증가
    List<Object> executeTransaction(String productId, Integer purchaseCount, boolean withWatch) {
        //리턴되는 List<Object> 에는
        //increment( "order#" + productId, purchaseCount );
        //decrement( "stock#" + productId, purchaseCount );
        //명령 수행 결과가 저장된다.
        return integerRedisTemplate.execute( new SessionCallback<>() {
            @Override
            //operations 파라미터에 integerRedisTemplate 인스턴스가 전달된다.
            //즉 operations 인스턴스와 integerRedisTemplate 인스턴스는 동일하다.
            //execute() 메서드 내에서는 redisTemplate 와 같은 다른 redisTemplate를 사용할 수 있다.
            public List<Object> execute( RedisOperations operations ) throws DataAccessException {
                if ( withWatch ) {
                    //watch를 통해서 stock#<productId> 키를 감시하지 않으면 currentStock을 가져온 뒤에 다른 프로세스를 통해
                    //stock#<productId> 가 변경되게 되면 데이터 일관성이 꺠지게 된다.
                    integerRedisTemplate.watch( "stock#" + productId );
                }

                Integer currentStock = integerRedisTemplate.opsForValue().get( "stock#" + productId );
                Assertions.assertThat( currentStock ).isNotNull();
                //현재 재고 수량이 구매 수량 보다 여유가 있는 경우
                if ( currentStock.compareTo( purchaseCount ) >= 0 ) {
                    //트랜잭션 시작
                    integerRedisTemplate.multi();
                    //주문의 구매 수량을 purchaseCount 만큼 증가
                    integerRedisTemplate.opsForValue().increment( "order#" + productId, purchaseCount );
                    //재고의 수량을 purchaseCount 만큼 감소
                    integerRedisTemplate.opsForValue().decrement( "stock#" + productId, purchaseCount );
                    //트랜잭션 command 실행
                    return integerRedisTemplate.exec();
                }
                //재고 수량이 부족한 경우
                else {
                    if ( withWatch ) {
                        integerRedisTemplate.unwatch();
                    }
                    return null;
                }
            }
        } );
    }
}
