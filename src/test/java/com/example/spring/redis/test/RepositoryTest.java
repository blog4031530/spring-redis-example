package com.example.spring.redis.test;

import com.example.spring.redis.model.Book;
import com.example.spring.redis.model.Person;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.data.redis.core.PartialUpdate;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class RepositoryTest extends BaseTest {

    UUID bookId;
    UUID authorId;

    @BeforeEach
    @DisplayName( "Book 인스턴스를 Repository를 통해서 저장" )
    void save_person() {
        //Person 클래스의 isNew() 메서드 결과로 새로운 엔티티 여부를 판단한다.
        Person author = Person.builder()
                .personName( "author" )
                .personAge( 75 )
                .personNation( "korea" )
                .comment( "my first book" )
                .build();
        author = personRepository.save( author );
        authorId = author.getId();
        Assertions.assertThat( authorId ).isNotNull();

        //기본적으로 @Id 필드에 값이 없으면 새로운 엔티티로 인식하여 자동으로 @Id 필드를 셋팅하여 저장한다.
        Book book = Book.builder()
                .title( "first book" )
                .pages( 100 )
                .author( author )
                .attr( Map.ofEntries(
                        Map.entry( "publisher", "dream world" ),
                        Map.entry( "publish date", "2024-01-01")) )
                .build();

        book = bookRepository.save( book );
        bookId = book.getBookId();
        Assertions.assertThat( bookId ).isNotNull();
    }

    @Test
    @DisplayName( "Repository를 통해서 book을 조회하는 테스트" )
    void repository_find_test() {
        //redis key 확인
        //RedisConfiguration에서 keyspace를 생성하도록 설정하였다. 이를 통해서 books:<bookId> 키에 book이 저장된다.
        Boolean hasKey = redisTemplate.hasKey( "books:" + bookId );
        Assertions.assertThat( hasKey ).isTrue();

        //bookId 조회
        Optional<Book> optionalBook = bookRepository.findById( bookId );
        Assertions.assertThat( optionalBook ).isPresent();

        Book book = optionalBook.get();
        Assertions.assertThat( book.getBookId() ).isEqualTo( bookId );

        //참조로 저장된 Person 인스턴스를 가져옴
        Person author = book.getAuthor();
        Assertions.assertThat( author.getId() ).isEqualTo( authorId );
        Assertions.assertThat( author.getPersonName() ).isEqualTo( "author" );
        Assertions.assertThat( author.getPersonAge() ).isEqualTo( 75 );
        Assertions.assertThat( author.getComment() ).isEqualTo( "my first book" );
        Assertions.assertThat( author.getPersonNation() ).isEqualTo( "korea" );

        //title 값 조회
        //title 조회를 위해서는 title 필드는 secondary index로 지정된 필드여야 함.
        List<Book> bookList = bookRepository.findByTitle( "first book" );
        Assertions.assertThat( bookList ).isNotEmpty();
        boolean isFind = bookList.stream().anyMatch( bookElement -> bookElement.getBookId().equals( bookId ) );
        Assertions.assertThat( isFind ).isTrue();
    }

    @Test
    @DisplayName( "PersonRepository를 통한 참조 필드 author를 조회 " )
    void find_by_author_test() {
        Optional<Book> optionalBook = bookRepository.findById( bookId );
        Assertions.assertThat( optionalBook ).isPresent();

        Book book = optionalBook.get();
        Optional<Person> optionalPerson = personRepository.findById( book.getAuthor().getId() );
        Assertions.assertThat( optionalPerson ).isPresent();
        Person person = optionalPerson.get();

        Assertions.assertThat( person.getId() ).isEqualTo( authorId );
        Assertions.assertThat( person.getPersonName() ).isEqualTo( "author" );
        Assertions.assertThat( person.getPersonAge() ).isEqualTo( 75 );
        Assertions.assertThat( person.getComment() ).isEqualTo( "my first book" );
        Assertions.assertThat( person.getPersonNation() ).isEqualTo( "korea" );
    }

    @Test
    @DisplayName( "Partial Update 테스트" )
    void partial_update_test() {
        PartialUpdate<Book> bookPartialUpdate = new PartialUpdate<>( bookId, Book.class )
                .set( "title", "updated first book" )
                .set( "pages", 1000 )
                .set( "attr.[publisher]", "partial updated dream world" );
        redisKeyValueAdapter.update( bookPartialUpdate );

        PartialUpdate<Person> personPartialUpdate = new PartialUpdate<>( authorId, Person.class )
                .set( "personName", "partial updated author" )
                .set( "personNation", "partial updated korea" )
                .set( "comment", "partial updated my first book" );
        redisKeyValueAdapter.update( personPartialUpdate );

        Optional<Book> optionalBook = bookRepository.findById( bookId );
        Assertions.assertThat( optionalBook ).isPresent();
        Book book = optionalBook.get();
        Assertions.assertThat( book.getBookId() ).isEqualTo( bookId );

        //book의 참조로 맵핑된 author Id에 해당하는 redis의 값이 변경된 정보를 가져온다.
        Person author = book.getAuthor();
        Assertions.assertThat( author.getId() ).isEqualTo( authorId );
        Assertions.assertThat( author.getPersonName() ).isEqualTo( "partial updated author" );
        Assertions.assertThat( author.getPersonNation() ).isEqualTo( "partial updated korea" );
        Assertions.assertThat( author.getComment() ).isEqualTo( "partial updated my first book" );
    }

    @Test
    @DisplayName( "id 를 지정하지 않은채로 book 인스턴스를 저장하는 테스트 (Entity State Detection Strategies)" )
    void entity_state_detection_test() {
        //id는 Null
        Person author = Person.builder()
                .personName( "author" )
                .personAge( 75 )
                .personNation( "korea" )
                .comment( "my first book" )
                .build();
        //entity state detection strategies에 의해서 ID가 null 이므로 새로운 것으로 간주하여
        //저장시 자동으로 ID를 생성한다.
        author = personRepository.save( author );

        //bookId는 null
        Book book = Book.builder()
                .bookId( bookId )
                .title( "first book" )
                .pages( 100 )
                .author( author )
                .build();

        //entity state detection strategies에 의해서 ID가 null 이므로 새로운 것으로 간주하여
        //저장시 자동으로 ID를 생성한다.
        book = bookRepository.save( book );

        Assertions.assertThat( book.getBookId() ).isNotNull();
        Assertions.assertThat( book.getAuthor().getId() ).isEqualTo( author.getId() );
    }

    @Test
    @DisplayName( "keyspace를 얻어오는 테스트" )
    void get_keyspace_test() {
        //keyspace는 redisKeyValueAdapter의 converter의 mapping context를 통해서 얻을 수 있다.
        String keySpace = redisKeyValueAdapter
                .getConverter()
                .getMappingContext()
                .getRequiredPersistentEntity( Book.class )
                .getKeySpace();
        Assertions.assertThat( keySpace ).isEqualTo( "books" );
    }

    @Test
    @DisplayName( "data mapping test" )
    void data_mapping_test() {
        String keySpace = redisKeyValueAdapter
                .getConverter()
                .getMappingContext()
                .getRequiredPersistentEntity( Book.class )
                .getKeySpace();

        Object attrPublisher = stringRedisTemplate.opsForHash().get( keySpace + ":" + bookId, "attr.[publisher]" );
        Assertions.assertThat( attrPublisher ).isNotNull();
        Assertions.assertThat( attrPublisher ).isEqualTo( "dream world" );
    }
}
