package com.example.spring.redis.test;

import com.example.spring.redis.configuration.RedisConfiguration;
import com.example.spring.redis.model.Person;
import com.example.spring.redis.repository.BookRepository;
import com.example.spring.redis.repository.PersonRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.redis.DataRedisTest;
import org.springframework.data.redis.core.RedisKeyValueAdapter;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith( SpringExtension.class )
@ContextConfiguration( classes = { RedisConfiguration.class } )
@DataRedisTest
@EnableAutoConfiguration
public class BaseTest {
    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    @Autowired
    RedisTemplate<String, Person> personRedisTemplate;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate<String, Integer> integerRedisTemplate;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    RedisKeyValueAdapter redisKeyValueAdapter;
}
