package com.example.spring.redis.service;

import com.example.spring.redis.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
@CacheConfig( cacheNames = "persons" )
public class PersonService {

    @Cacheable( key = "#personId" )
    public Person findPerson( UUID personId ) {
        log.info( "findPerson called, personId={}", personId );
        //아래 로직이 실행된다는 것은 persons 캐시에 personId 식별자를 가진 엔트리가 존재하지 않는 것을 의미.
        //보통은 DB에서 조회하는 로직이 들어가지만 편의상 새로운 Person 인스턴스를 생성하여 리턴한다.
        return Person.builder()
                .id( personId )
                .personName( "cached-person" )
                .personNation( "korea" )
                .personAge( 100 )
                .comment( "cached data" )
                .build();
    }

    @CachePut( key = "#person.id" )
    public Person savePerson( Person person ) {
        log.info( "savePerson called, person={}", person );
        //보통은 DB에 업데이트하는 로직이 들어가지만 편의상 person 인스턴스를 그대로 리턴한다.
        //리턴되는 인스턴스는 cache 에 업데이트 된다.
        return person;
    }

    @CachePut( key = "#person.id" )
    public Person updatePerson( Person person ) {
        log.info( "updatePerson called, person={}", person );
        //보통은 DB에 업데이트하는 로직이 들어가지만 편의상 person 인스턴스를 그대로 리턴한다.
        //리턴되는 인스턴스는 cache 에 업데이트 된다.
        return person;
    }

    @CacheEvict( key = "#personId" )
    public void deletePerson( UUID personId ) {
        log.info( "deletePerson called, personId={}", personId );
        //보통은 DB에서 데이터를 삭제하는 로직이 들어가지만 편의상 로그만 생성한다.
        //메서드 실행이 완료되면 personId 에 해당하는 엔트리가 캐시에서 삭제된다.
    }
}
