package com.example.spring.redis.controller;

import com.example.spring.redis.model.Person;
import com.example.spring.redis.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/persons")
@RequiredArgsConstructor
public class PersonController {
    private final PersonService personService;

    @GetMapping(path = "/{personId}", produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<Person> getPerson( @PathVariable UUID personId ) {
        Person person = personService.findPerson( personId );
        return ResponseEntity.ok( person );
    }

    @PostMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<Person> createPerson( @RequestBody Person person ) {
        Person savedPerson = personService.savePerson( person );
        return ResponseEntity.ok( savedPerson );
    }

    @PutMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> updatePerson( @RequestBody Person person ) {
        Person updatedPerson = personService.updatePerson( person );
        return ResponseEntity.ok( updatedPerson );
    }

    @DeleteMapping(path = "/{personId}")
    public ResponseEntity<Void> deletePerson( @PathVariable UUID personId ) {
        personService.deletePerson( personId );
        return ResponseEntity.noContent().build();
    }
}
