package com.example.spring.redis.repository;

import com.example.spring.redis.model.Book;
import org.springframework.data.repository.core.support.AbstractEntityInformation;

import java.util.UUID;

public class BookEntityInformation extends AbstractEntityInformation<Book, UUID> {
    public BookEntityInformation() {
        super( Book.class );
    }

    @Override
    public UUID getId( Book entity ) {
        return entity.getBookId();
    }

    @Override
    public Class<UUID> getIdType() {
        return UUID.class;
    }
}
