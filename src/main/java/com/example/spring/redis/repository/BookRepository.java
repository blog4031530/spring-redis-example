package com.example.spring.redis.repository;

import com.example.spring.redis.model.Book;
import org.springframework.data.keyvalue.repository.KeyValueRepository;

import java.util.List;
import java.util.UUID;

public interface BookRepository extends KeyValueRepository<Book, UUID> {
    //title로 조회하기 위해서는 title 필드에 @Indexed가 지정되어 있어야 함. (secondary index)
    List<Book> findByTitle(String title);
}
