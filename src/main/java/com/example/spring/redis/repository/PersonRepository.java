package com.example.spring.redis.repository;

import com.example.spring.redis.model.Person;
import org.springframework.data.keyvalue.repository.KeyValueRepository;

import java.util.List;
import java.util.UUID;

public interface PersonRepository extends KeyValueRepository<Person, UUID> {
    List<Person> findByPersonName(String personName);
}
