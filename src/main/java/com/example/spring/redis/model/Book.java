package com.example.spring.redis.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Reference;

import java.util.Map;
import java.util.UUID;

//configuration 에서 keyspace를 직접 정의하여 @RedisHash를 지정하지 않았다.
//@RedisHash()
@Getter
@Setter
public class Book {
    @Id
    private UUID bookId;
    //configuration 에 의해서 title 인덱스 생성으로 @Indexed는 제거하였다.
    //@Indexed
    private String title;
    //@Reference로 지정된 필드에는 @Indexed 어노테이션이 적용되지 않는다.
    //Person 인스턴스의 참조
    @Reference
    private Person author;
    private int pages;
    //@Indexed 를 지정하면 전체 필드에 대해서 secondary index가 생성된다.
    //attr 에 저장된 데이터 수가 많으면 redis 성능에 좋지 않은 영향을 줄 수 있다.
    //만약 attr 의 특정 키에 대해서 secondary index를 지정하려면 configuration 으로 지정해야 한다.
    private Map<String, String> attr;

    @Builder
    public Book(UUID bookId, String title, Person author, int pages, Map<String, String> attr) {
        this.bookId = bookId;
        this.title = title;
        this.author = author;
        this.pages = pages;
        this.attr = attr;
    }
}
