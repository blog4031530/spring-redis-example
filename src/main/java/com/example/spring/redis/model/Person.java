package com.example.spring.redis.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;

//configuration 에서 keyspace를 직접 정의하여 @RedisHash를 지정하지 않았다.
//@RedisHash()
@Getter
@Setter
@NoArgsConstructor
public class Person extends Persistent {
    private String personName;
    private Integer personAge;
    private String personNation;
    private String comment;

    @Builder
    public Person( UUID id, String personName, Integer personAge, String personNation, String comment ) {
        super(id);
        this.personName = personName;
        this.personAge = personAge;
        this.personNation = personNation;
        this.comment = comment;
    }

    public Map<String, Object> createMap() {
        return Map.ofEntries(
                Map.entry( "id", this.id ),
                Map.entry( "personName", this.personName ),
                Map.entry( "personAge", this.personAge ),
                Map.entry( "personNation", this.personNation ),
                Map.entry( "comment", this.comment )
        );
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        Person person = ( Person )o;
        return Objects.equals( id, person.id );
    }

    @Override
    public int hashCode() {
        return Objects.hashCode( id );
    }
}
