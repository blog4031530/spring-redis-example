package com.example.spring.redis.configuration;

import com.example.spring.redis.model.Book;
import com.example.spring.redis.model.Person;
import io.lettuce.core.event.DefaultEventPublisherOptions;
import io.lettuce.core.metrics.CommandLatencyRecorder;
import io.lettuce.core.resource.Delay;
import lombok.RequiredArgsConstructor;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.boot.autoconfigure.cache.RedisCacheManagerBuilderCustomizer;
import org.springframework.boot.autoconfigure.data.redis.ClientResourcesBuilderCustomizer;
import org.springframework.boot.autoconfigure.data.redis.LettuceClientConfigurationBuilderCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.convert.KeyspaceConfiguration;
import org.springframework.data.redis.core.index.IndexConfiguration;
import org.springframework.data.redis.core.index.IndexDefinition;
import org.springframework.data.redis.core.index.SimpleIndexDefinition;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.time.Duration;
import java.util.Set;

@Configuration
@RequiredArgsConstructor
@EnableRedisRepositories(basePackages = "com.example.spring.redis.repository",
    indexConfiguration = RedisConfiguration.SecondaryIndexConfiguration.class,
    keyspaceConfiguration = RedisConfiguration.CustomKeyspaceConfiguration.class )
@EnableCaching
public class RedisConfiguration {

    @Bean
    public ClientResourcesBuilderCustomizer clientResourceCustomize() {
        return builder -> {
            builder.reconnectDelay( Delay.constant( Duration.ofSeconds(50) ) );
            builder.ioThreadPoolSize( 4 );
            builder.commandLatencyRecorder( CommandLatencyRecorder.disabled() );
            builder.commandLatencyPublisherOptions( DefaultEventPublisherOptions.disabled() );
        };
    }

    @Bean
    public LettuceClientConfigurationBuilderCustomizer lettucyClientConfigCustomize() {
        return builder -> {
            if ( builder instanceof
                    LettucePoolingClientConfiguration.LettucePoolingClientConfigurationBuilder poolingBuilder ) {
                // Pool configuration
                GenericObjectPoolConfig<?> poolConfig = new GenericObjectPoolConfig<>();
                poolConfig.setMaxIdle(10);
                poolConfig.setMinIdle(5);
                poolConfig.setMaxTotal(20);
                poolConfig.setMaxWait(Duration.ofSeconds( 30 ));
                poolingBuilder.poolConfig( poolConfig );
            }

            builder.shutdownTimeout( Duration.ofMillis( 1000L ) );

        };
    }

    // lettuceConnectionFactory 인스턴스는 spring data redis auto configuration에 의해서 자동 생성됨
    @Bean
    public RedisTemplate<String, Object> redisTemplate( LettuceConnectionFactory lettuceConnectionFactory ) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory( lettuceConnectionFactory );

        // set key serializer
        // template.setKeySerializer( new StringRedisSerializer(StandardCharsets.UTF_8) ); 와 동일함
        template.setKeySerializer( RedisSerializer.string() );
        template.setHashKeySerializer( RedisSerializer.string() );

        // set value serializer
        // template.setDefaultSerializer( new GenericJackson2JsonRedisSerializer() ); 와 동일함
        template.setDefaultSerializer( RedisSerializer.json() );
        template.setValueSerializer( RedisSerializer.json() );
        template.setHashValueSerializer( RedisSerializer.json() );

        return template;
    }

    @Bean
    public RedisTemplate<String, Person> personRedisTemplate( LettuceConnectionFactory lettuceConnectionFactory ) {
        RedisTemplate<String, Person> template = new RedisTemplate<>();
        template.setConnectionFactory( lettuceConnectionFactory );

        // set key serializer
        // template.setKeySerializer( new StringRedisSerializer(StandardCharsets.UTF_8) ); 와 동일함
        template.setKeySerializer( RedisSerializer.string() );
        template.setHashKeySerializer( RedisSerializer.string() );

        // set value serializer
        // template.setDefaultSerializer( new GenericJackson2JsonRedisSerializer() ); 와 동일함
        template.setValueSerializer( RedisSerializer.json() );
        template.setHashValueSerializer( RedisSerializer.json() );

        return template;
    }

    //String 타입 value 전용 RedisTemplate 빈 생성
    @Bean
    public StringRedisTemplate stringRedisTemplate( LettuceConnectionFactory lettuceConnectionFactory ) {
        return new StringRedisTemplate(lettuceConnectionFactory);
    }

    //Integer 타입 value 전용 RedisTemplate 빈 생성
    @Bean
    public RedisTemplate<String, Integer> integerRedisTemplate( LettuceConnectionFactory lettuceConnectionFactory ) {
        RedisTemplate<String, Integer> template = new RedisTemplate<>();
        template.setConnectionFactory( lettuceConnectionFactory );
        // set key serializer
        // template.setKeySerializer( new StringRedisSerializer(StandardCharsets.UTF_8) ); 와 동일함
        template.setKeySerializer( RedisSerializer.string() );
        template.setHashKeySerializer( RedisSerializer.string() );

        // set value serializer
        // template.setDefaultSerializer( new GenericJackson2JsonRedisSerializer() ); 와 동일함
        template.setValueSerializer( RedisSerializer.json() );
        template.setHashValueSerializer( RedisSerializer.json() );
        return template;
    }

    /*@Bean
    public RedisCacheManager cacheManager( RedisConnectionFactory redisConnectionFactory,
                                           CacheProperties cacheProperties ) {
        RedisCacheConfiguration cacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                .computePrefixWith( CacheKeyPrefix.prefixed( cacheProperties.getRedis().getKeyPrefix() ) )
                .serializeKeysWith( RedisSerializationContext.SerializationPair.fromSerializer( RedisSerializer.string() ) )
                .serializeValuesWith( RedisSerializationContext.SerializationPair.fromSerializer( RedisSerializer.json() ) );

        return RedisCacheManager.builder(redisConnectionFactory)
                .cacheDefaults( cacheConfiguration ).build();
    }*/

    //기본적으로 자동 구성되는 RedisCacheManager 에 RedisCacheConfiguration 만 커스텀하게 변경할 수 있다.
    @Bean
    public RedisCacheManagerBuilderCustomizer redisCacheManagerBuilderCustomizer() {
        //serializer 를 GenericJackson2JsonRedisSerializer 로 변경.
        return builder -> {
            RedisCacheConfiguration cacheConfiguration = builder.cacheDefaults()
                    .serializeKeysWith( RedisSerializationContext.SerializationPair.fromSerializer( RedisSerializer.string() ) )
                    .serializeValuesWith( RedisSerializationContext.SerializationPair.fromSerializer( RedisSerializer.json() ) );

            builder.cacheDefaults( cacheConfiguration );
        };
    }

    /*//keyspace, secondary index 직접 설정
    @Bean
    public RedisMappingContext mappingContext() {
        return new RedisMappingContext(
                new MappingConfiguration( new SecondaryIndexConfiguration(), new CustomKeyspaceConfiguration() )
        );
    }

    //RedisKeyValueAdapter 빈 직접 생성
    @Bean
    public RedisKeyValueAdapter redisKeyValueAdapter( RedisTemplate<String, Object> redisTemplate,
                                                      RedisMappingContext mappingContext) {
        //mappingRedisConverter 인스턴스를 전달해도 무방하다.
        //MappingRedisConverter mappingRedisConverter = new MappingRedisConverter( mappingContext );
        //return new RedisKeyValueAdapter(redisTemplate, mappingRedisConverter);
        return new RedisKeyValueAdapter(redisTemplate, mappingContext);
    }

    //RedisKeyValueTemplate 빈 생성
    //SimpleKeyValueRepository에서 사용되는 RedisKeyValueTemplate 에도 MappingContext가 전달되어야
    //데이터 조회시 keyspace를 찾는다.
    @Bean
    public RedisKeyValueTemplate redisKeyValueTemplate( RedisKeyValueAdapter redisKeyValueAdapter,
                                                        RedisMappingContext mappingContext ) {
        return new RedisKeyValueTemplate( redisKeyValueAdapter, mappingContext );
    }*/

    //secondary index 생성을 위한 IndexConfiguration 확장 클래스 정의
    public static class SecondaryIndexConfiguration extends IndexConfiguration {
        @Override
        protected Iterable<? extends IndexDefinition> initialConfiguration() {
            return Set.of(
                    new SimpleIndexDefinition( "persons", "personName" ),
                    new SimpleIndexDefinition( "books", "title" ),
                    new SimpleIndexDefinition( "books", "attr.publisher" )
            );
        }
    }

    //keyspace 생성을 위한 KeyspaceConfiguration 확장 클래스 정의
    public static class CustomKeyspaceConfiguration extends KeyspaceConfiguration {
        @Override
        protected Iterable<KeyspaceSettings> initialConfiguration() {
            return Set.of(
                    new KeyspaceSettings( Person.class, "persons" ),
                    new KeyspaceSettings( Book.class, "books" )
            );
        }
    }
}
