* spring boot docker compose support를 사용하여 샘플 코드를 실행하기 위해서는 Docker 엔진이 실행되고 있어야 합니다.
* JDK 버전은 21 입니다.
--- 
[Spring Data Redis - Auto Configuration을 이용한 Redis 연결 설정](https://devel-repository.tistory.com/82)

[Spring Data Redis - RedisTemplate의 ValueOperations](https://devel-repository.tistory.com/84)

[Spring Data Redis - RedisTemplate 트랜잭션](https://devel-repository.tistory.com/85)

[Spring Data Redis - RedisTemplate의 HashOperations](https://devel-repository.tistory.com/86)

[Spring Data Redis - Redis Repository 사용](https://devel-repository.tistory.com/87)

[Spring Data Redis - cache 기능 사용하기](https://devel-repository.tistory.com/90)